import { animate, animateChild, group, query, style, transition, trigger } from "@angular/animations";

export const routeTransitionAnimations = trigger("triggerName", [
  transition("signin => signup, signup => signin", [
    query(":enter, :leave", [
      style({
        position: "absolute",
        top: 0,
        right: 0,
        width: "100%"
      })
    ]),
    query(":enter", [style({ opacity: 0 })]),
    query(":leave", animateChild()),
    group([
      query(":leave", [animate(".5s ease-out", style({ opacity: 0 }))]),
      query(":enter", [animate(".5s ease-out", style({ opacity: 1 }))])
    ]),
    query(":enter", animateChild())
  ])

]);
