import { LanguageCodes } from "../Enums/language-codes";

export interface Flashcard {
  id: string;
  keyWord: string;
  translation: string;
  originLanguage: LanguageCodes;
  translationLanguage: LanguageCodes;
}
