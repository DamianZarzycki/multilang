export interface CommunicationEvent {
  key: string;
  data?: any;
}
