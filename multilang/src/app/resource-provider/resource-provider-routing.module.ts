import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PageNotFoundComponent } from "../shared-module/navigation/page-not-found/page-not-found.component";
import { EntryPointResourceProviderComponent } from "./entry-point-resource-provider/entry-point-resource-provider.component";


const routes: Routes = [
  {
    path: "",
    component: EntryPointResourceProviderComponent
  },
  {
    path: "**",
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResourceProviderRoutingModule {
}
