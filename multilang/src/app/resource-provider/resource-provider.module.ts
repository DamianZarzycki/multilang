import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EntryPointResourceProviderComponent } from "./entry-point-resource-provider/entry-point-resource-provider.component";
import { ResourceProviderRoutingModule } from "./resource-provider-routing.module";


@NgModule({
  declarations: [
    EntryPointResourceProviderComponent],
  imports: [
    CommonModule,
    ResourceProviderRoutingModule
  ]
})
export class ResourceProviderModule {
}
