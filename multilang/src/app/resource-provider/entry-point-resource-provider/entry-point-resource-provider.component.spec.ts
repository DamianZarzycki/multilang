import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryPointResourceProviderComponent } from './entry-point-resource-provider.component';

describe('EntryPointResourceProviderComponent', () => {
  let component: EntryPointResourceProviderComponent;
  let fixture: ComponentFixture<EntryPointResourceProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryPointResourceProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryPointResourceProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
