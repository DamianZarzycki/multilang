import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SignInFormComponent } from "./components/signing/sign-in-form/sign-in-form.component";
import { SignUpFormComponent } from "./components/signing/sign-up-form/sign-up-form.component";
import { PageNotFoundComponent } from "./shared-module/navigation/page-not-found/page-not-found.component";
import { MainComponent } from "./components/main/main.component";
import { EntryPointLearningComponent } from "./learning/entry-point-learning/entry-point-learning.component";


const routes: Routes = [
  {
    path: "", children: [
      {
        path: "",
        redirectTo: "signin",
        pathMatch: "full"
      },
      {
        path: "signin", component: SignInFormComponent,
        data: {
          animationState: 'signin'
        }
      },
      {
        path: "signup", component: SignUpFormComponent,
        data: {
          animationState: 'signup'
        }
      }
    ]
  },
  {
    path: "user/:id", component: MainComponent, children: [
      {
        path: "learning",
        data: {
          breadcrumb: {
            label: "Learning"
          }
        },
        loadChildren: () => import("./learning/learning.module").then(m => m.LearningModule)
      },
      {
        path: "resources",
        data: {
          breadcrumb: {
            label: "Resources"
          }
        },
        loadChildren: () => import("./resource-provider/resource-provider.module").then(m => m.ResourceProviderModule)
      }
    ]
  },
  {
    path: "**", component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
