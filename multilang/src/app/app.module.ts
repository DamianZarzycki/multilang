import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SignInFormComponent } from "./components/signing/sign-in-form/sign-in-form.component";
import { SignUpFormComponent } from "./components/signing/sign-up-form/sign-up-form.component";
import { NavigationBarComponent } from "./shared-module/navigation/navigation-bar/navigation-bar.component";
import { PageNotFoundComponent } from "./shared-module/navigation/page-not-found/page-not-found.component";
import { MainComponent } from "./components/main/main.component";
import { ButtonModule } from "primeng/button";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { BreadcrumbComponent } from "./shared-module/navigation/breadcrumb/breadcrumb.component";
import { FormBuilder, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { InputTextModule } from "primeng/inputtext";
import { LogoComponent } from "./components/signing/logo/logo.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CoreModule } from "./core-module/core.module";
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    SignInFormComponent,
    SignUpFormComponent,
    NavigationBarComponent,
    PageNotFoundComponent,
    MainComponent,
    BreadcrumbComponent,
    LogoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonModule,
    BreadcrumbModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    BrowserAnimationsModule,
    CoreModule,
    HttpClientModule
  ],
  providers: [FormBuilder],
  bootstrap: [AppComponent]
})
export class AppModule { }
