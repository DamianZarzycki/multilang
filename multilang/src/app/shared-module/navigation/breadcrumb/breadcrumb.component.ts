import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../../core-module/services/breadcrumb.service";
import { MenuItem } from "primeng/api";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { filter, map, mergeMap } from "rxjs/operators";

@Component({
  selector: "app-breadcrumb",
  templateUrl: "./breadcrumb.component.html",
  styleUrls: ["./breadcrumb.component.scss"]
})
export class BreadcrumbComponent implements OnInit{

  breadcrumbSource: MenuItem[];
  home: MenuItem;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
  }

  private setBreadCrumb() {
    this.breadcrumbSource = [{
      label: "MultiLang"
    }];
    this.home = { icon: "pi pi-home" };
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map((route) => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        mergeMap((route) => route.data))
      .subscribe((event: { breadcrumb: { label: string } }) => {
        this.createBreadcrumb(event.breadcrumb);
      });
  }

  private createBreadcrumb(newSource: MenuItem) {
    const breadcrumbFirstEl = {
      label: "MultiLang"
    };
    Array.isArray(newSource) ? this.breadcrumbSource = [breadcrumbFirstEl, ...newSource] :
      this.breadcrumbSource = [breadcrumbFirstEl, newSource];
  }

  ngOnInit(): void {
    this.setBreadCrumb();

  }

}
