import { Component, EventEmitter, OnInit, Output } from "@angular/core";

@Component({
  selector: "app-side-bar",
  templateUrl: "./side-bar.component.html",
  styleUrls: ["./side-bar.component.scss"]
})
export class SideBarComponent implements OnInit {
  @Output() categoryReceived = new EventEmitter<string>();

  displaySideBar = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  onClick() {
    this.displaySideBar ? this.hideSideBar() : this.showSideBar();
  }

  onCategorySelected(event) {
    this.categoryReceived.emit(event);
  }

  private hideSideBar() {
    this.displaySideBar = false;
  }

  private showSideBar() {
    this.displaySideBar = true;
  }

}
