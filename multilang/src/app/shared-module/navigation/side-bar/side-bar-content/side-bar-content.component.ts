import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { MenuItem } from "primeng/api";

@Component({
  selector: "app-side-bar-content",
  templateUrl: "./side-bar-content.component.html",
  styleUrls: ["./side-bar-content.component.scss"]
})
export class SideBarContentComponent implements OnInit {

  @Output() categorySelected = new EventEmitter<string>();
  items: MenuItem[];
  unselectedStateIconName = "pi-circle-off";
  selectedStateIconName = "pi-circle-on";
  folderIconBaseName = "pi pi-fw";

  constructor() {
  }

  ngOnInit() {
    this.items = [
      {
        id: "0",
        label: "Education",
        icon: `${this.folderIconBaseName} ${this.unselectedStateIconName}`,
        command: event => {
          console.log(event);
          this.changeFolderIcon(event.item);
          this.categorySelected.emit(event.item.label);

        }
      }, {
        id: "1",
        label: "Technology",
        icon: `${this.folderIconBaseName} ${this.unselectedStateIconName}`,
        command: event => {
          console.log(event);
          this.changeFolderIcon(event.item);
          this.categorySelected.emit(event.item.label);

        }
      },
      {
        id: "2",
        label: "Animals",
        icon: `${this.folderIconBaseName} ${this.unselectedStateIconName}`,
        command: event => {
          console.log(event);
          this.changeFolderIcon(event.item);
          this.categorySelected.emit(event.item.label);

        }
      }
    ];
  }

  private changeFolderIcon(menuItem: MenuItem) {
    this.items.map(e => {
      const newIconName = e.id === menuItem.id ? this.selectedStateIconName : this.unselectedStateIconName;
      e.icon = `${this.folderIconBaseName} ${newIconName}`;
    });
  }

}
