import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextToSpeechButtonComponent } from './text-to-speech-button.component';

describe('TextToSpeechButtonComponent', () => {
  let component: TextToSpeechButtonComponent;
  let fixture: ComponentFixture<TextToSpeechButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextToSpeechButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextToSpeechButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
