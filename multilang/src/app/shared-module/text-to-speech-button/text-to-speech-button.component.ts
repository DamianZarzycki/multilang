import { Component, Input, OnInit } from "@angular/core";
import { TextToSpeechService } from "../../core-module/services/text-to-speech.service";
import { EventService } from "../../core-module/services/event.service";
import { EventNames } from "../../models/Interfaces/event-names";
import { LanguageCodes } from "../../models/Enums/language-codes";

@Component({
  selector: "app-text-to-speech-button",
  templateUrl: "./text-to-speech-button.component.html",
  styleUrls: ["./text-to-speech-button.component.scss"]
})
export class TextToSpeechButtonComponent implements OnInit {
  @Input() language: string;

  constructor(private textToSpeechService: TextToSpeechService, private eventService: EventService) {
  }

  ngOnInit(): void {
    this.eventService.getEvent(EventNames.eventNames.speechToText.eng).subscribe(() => {
      this.textToSpeechService.currentLanguageCode = LanguageCodes.EnUS;
    });
  }

  onClick() {
    this.textToSpeechService.textToSpeech();
  }

}
