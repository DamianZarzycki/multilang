import { Directive, ElementRef, HostBinding, Renderer2 } from "@angular/core";

@Directive({
  selector: "[appCursorPointer]"
})
export class CursorPointerDirective {
  domElement: any;

  @HostBinding("style.cursor") cursor = "pointer";

  constructor(private renderer: Renderer2,
              private elementRef: ElementRef) {
    this.domElement = this.elementRef.nativeElement;
    const requiredStyles = {
      cursor: "pointer"
    };

    Object.keys(requiredStyles).forEach(newStyle => {
      this.renderer.setStyle(
        this.domElement, `${newStyle}`, requiredStyles[newStyle]
      );
    });
  }
}
