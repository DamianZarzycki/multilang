import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ButtonModule } from "primeng/button";
import { RippleModule } from "primeng/ripple";
import { CursorPointerDirective } from "./directives/cursor-pointer.directive";
import {MenuModule} from 'primeng/menu';
import { SideBarContentComponent } from './navigation/side-bar/side-bar-content/side-bar-content.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    RippleModule,
    MenuModule
  ],
  declarations: [
    CursorPointerDirective,
    SideBarContentComponent],
  exports: [
    CommonModule, CursorPointerDirective, SideBarContentComponent
  ]
})
export class SharedModule {
}
