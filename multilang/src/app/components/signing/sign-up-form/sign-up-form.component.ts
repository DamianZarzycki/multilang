import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-sign-up-form",
  templateUrl: "./sign-up-form.component.html",
  styleUrls: ["./sign-up-form.component.scss"]
})
export class SignUpFormComponent implements OnInit {
  signUpForm: FormGroup;
  showPassword = false;
  showConfirmPassword = false;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.signUpForm = this.fb.group({
      userName: ["", [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(30)]],
      confirmPassword: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(30)]]
    });
  }

  onSubmit() {
    console.log("Submiting...");
  }

  changePasswordInputType() {
    this.showPassword = !this.showPassword;
  }

  changeConfirmPasswordInputType() {
    this.showConfirmPassword = !this.showConfirmPassword;

  }


}
