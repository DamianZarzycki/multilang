import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: 'multilang-logo',
  styleUrls: ['./logo.component.scss'],
  templateUrl: './logo.component.html'
})
export class LogoComponent implements OnInit {
  @Input() navbar = false;
  constructor() {
  }

  ngOnInit(): void {
  }

}
