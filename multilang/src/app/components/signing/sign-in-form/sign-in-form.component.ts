import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { max } from "rxjs/operators";

@Component({
  selector: "app-sign-in-form",
  templateUrl: "./sign-in-form.component.html",
  styleUrls: ["./sign-in-form.component.scss"]
})
export class SignInFormComponent implements OnInit {
  signInForm: FormGroup;
  showPassword = false;

  constructor(private fb: FormBuilder) {
  }


  ngOnInit(): void {
    this.signInForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(30)]]
    });
  }

  changePasswordInputType() {
    this.showPassword = !this.showPassword;
  }

  onSubmit() {
    console.log("Submiting...");
  }

}
