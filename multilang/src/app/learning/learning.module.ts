import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EntryPointLearningComponent } from "./entry-point-learning/entry-point-learning.component";
import { LearningRoutingModule } from "./learning-routing.module";
import { SharedModule } from "../shared-module/shared.module";
import { SideBarComponent } from "../shared-module/navigation/side-bar/side-bar.component";
import { FlashcardComponent } from "./flashcard/flashcard.component";
import { FlashcardFrontComponent } from "./flashcard/flashcard-front/flashcard-front.component";
import { FlashcardBackComponent } from "./flashcard/flashcard-back/flashcard-back.component";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { ButtonModule } from "primeng/button";
import { RippleModule } from "primeng/ripple";
import { TextToSpeechButtonComponent } from "../shared-module/text-to-speech-button/text-to-speech-button.component";


@NgModule({
  declarations: [EntryPointLearningComponent, SideBarComponent, FlashcardComponent, FlashcardFrontComponent, FlashcardBackComponent, TextToSpeechButtonComponent],
  imports: [
    CommonModule,
    LearningRoutingModule,
    SharedModule,
    ScrollPanelModule,
    ButtonModule,
    RippleModule,
  ]
})
export class LearningModule { }
