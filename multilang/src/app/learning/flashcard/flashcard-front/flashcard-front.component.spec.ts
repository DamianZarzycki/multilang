import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlashcardFrontComponent } from './flashcard-front.component';

describe('FlashcardFrontComponent', () => {
  let component: FlashcardFrontComponent;
  let fixture: ComponentFixture<FlashcardFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlashcardFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlashcardFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
