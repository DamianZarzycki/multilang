import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "app-flashcard-front",
  templateUrl: "./flashcard-front.component.html",
  styleUrls: ["./flashcard-front.component.scss"]
})
export class FlashcardFrontComponent implements OnInit {
  @Input() keyWord: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
