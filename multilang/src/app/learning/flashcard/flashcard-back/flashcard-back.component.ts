import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: 'app-flashcard-back',
  templateUrl: './flashcard-back.component.html',
  styleUrls: ['./flashcard-back.component.scss']
})
export class FlashcardBackComponent implements OnInit {
  @Input() keyWordTranslation: string;

  constructor() { }

  ngOnInit(): void {
  }

}
