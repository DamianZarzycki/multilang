import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from "@angular/core";
import { CardData } from "../../models/Interfaces/card-data";
import { cardFlipAnimation } from "../../animations/card-flip-animation";
import { TextToSpeechService } from "../../core-module/services/text-to-speech.service";
import { LanguageCodes } from "../../models/Enums/language-codes";
import { RestService } from "../../core-module/services/rest.service";
import { Flashcard } from "../../models/Interfaces/flashcard";
import { EventService } from "../../core-module/services/event.service";
import { FlashcardService } from "../../core-module/services/flashcard.service";

@Component({
  selector: "app-flashcard",
  templateUrl: "./flashcard.component.html",
  styleUrls: ["./flashcard.component.scss"],
  animations: [cardFlipAnimation]
})
export class FlashcardComponent implements OnInit, OnChanges {
  @Input() currentFlashcardIdx: number;
  @Output() flashcardsReceived = new EventEmitter<Flashcard[]>();
  keyWord: string;
  keyWordTranslation: string;
  flipped = false;
  flashcards: Flashcard[] = [];
  data: CardData = {
    state: "default"
  };

  constructor(private textToSpeechService: TextToSpeechService,
              private restService: RestService,
              private eventService: EventService,
              private flashcardService: FlashcardService) {
  }

  ngOnInit(): void {
    this.flashcardService.getFlashcardState().subscribe((state) => {
      if (this.data.state === "default" && state === "flipped") {
        this.data.state = "flipped";
      } else if (this.data.state === "flipped" && state === "default") {
        this.data.state = "default";
      }
    });

    this.restService.getFlashcards().subscribe((response) => {
      if (response.status === 200) {
        this.flashcards = response.body;
        this.keyWord = this.flashcards[this.currentFlashcardIdx].keyWord;
        this.keyWordTranslation = this.flashcards[this.currentFlashcardIdx].translation;
        this.flashcardsReceived.emit(this.flashcards);
      }
    });
  }

  onCardClicked() {
    if (this.data.state === "default") {
      this.flashcardService.setFlashcardState("flipped");
      this.textToSpeechService.currentLanguageCode = LanguageCodes.plPL;
      this.textToSpeechService.textToRead = this.flashcards[this.currentFlashcardIdx].translation;
    } else {
      this.flashcardService.setFlashcardState("default");
      this.textToSpeechService.currentLanguageCode = LanguageCodes.EnUS;
      this.textToSpeechService.textToRead = this.flashcards[this.currentFlashcardIdx].keyWord;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.flashcardService.setFlashcardState("default");
    this.textToSpeechService.currentLanguageCode = LanguageCodes.EnUS;
  }

}
