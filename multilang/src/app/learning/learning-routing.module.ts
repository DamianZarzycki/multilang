import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EntryPointLearningComponent } from "./entry-point-learning/entry-point-learning.component";
import { PageNotFoundComponent } from "../shared-module/navigation/page-not-found/page-not-found.component";


const routes: Routes = [
  {
    path: "",
    component: EntryPointLearningComponent
  },
  {
    path: "**",
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LearningRoutingModule {
}
