import { Component, OnDestroy, OnInit } from "@angular/core";
import { Flashcard } from "../../models/Interfaces/flashcard";
import { TextToSpeechService } from "../../core-module/services/text-to-speech.service";
import { EventService } from "../../core-module/services/event.service";
import { EventNames } from "../../models/Interfaces/event-names";
import { FlashcardService } from "../../core-module/services/flashcard.service";

@Component({
  selector: "app-entry-point-learning",
  templateUrl: "./entry-point-learning.component.html",
  styleUrls: ["./entry-point-learning.component.scss"]
})
export class EntryPointLearningComponent implements OnInit, OnDestroy {
  // TODO maybe enum in future or interface
  isCategorySelected = false;
  selectedCategory = "Select category";
  flashcards: Flashcard[] = [];
  language = null;
  idx = 0;

  constructor(
    private textToSpeechService: TextToSpeechService,
    private eventService: EventService,
    private flashcardService: FlashcardService) {
  }

  ngOnInit(): void {
  }

  onCategoryReceived(event) {
    this.isCategorySelected = true;
    this.selectedCategory = event;
    this.flashcardService.setFlashcardState("default");
    this.eventService.forwardEvent({ key: EventNames.eventNames.speechToText.eng });
    if (this.flashcards.length >= 1) {
      this.textToSpeechService.textToRead = this.flashcards[this.idx].keyWord;
    }
  }

  onFlashcardsReceived(event) {
    this.flashcards = event;
    if (this.flashcards[this.idx].keyWord) {
      this.textToSpeechService.textToRead = this.flashcards[this.idx].keyWord;
    }
  }

  selectAdjacentFlashcard(state: "next" | "previous") {
    if (state === "next") {
      if (this.idx < this.flashcards.length - 1) {
        this.idx++;
      } else {
        return;
      }
    } else if (state === "previous") {
      if (this.idx > 0) {
        this.idx--;
      } else {
        return;
      }
    }
    this.textToSpeechService.textToRead = this.flashcards[this.idx].keyWord;
  }

  ngOnDestroy(): void {
    this.isCategorySelected = false;
  }
}
