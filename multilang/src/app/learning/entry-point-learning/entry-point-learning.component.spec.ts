import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryPointLearningComponent } from './entry-point-learning.component';

describe('EntryPointLearningComponent', () => {
  let component: EntryPointLearningComponent;
  let fixture: ComponentFixture<EntryPointLearningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryPointLearningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryPointLearningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
