import { TestBed } from '@angular/core/testing';

import { StaticUrlInterceptor } from './static-url.interceptor';

describe('StaticUrlInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      StaticUrlInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: StaticUrlInterceptor = TestBed.inject(StaticUrlInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
