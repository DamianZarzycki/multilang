import { Injectable } from "@angular/core";
import { NgEventBus } from "ng-event-bus";
import { CommunicationEvent } from "../../models/Interfaces/communication-event";

@Injectable({
  providedIn: "root"
})
export class EventService {

  constructor(private  ngEventBus: NgEventBus) {
  }

  forwardEvent(eventData: CommunicationEvent) {
    if (eventData.data) {
      this.ngEventBus.cast(eventData.key, eventData.data);
    } else {
      this.ngEventBus.cast(eventData.key);
    }
  }

  getEvent(name: string) {
    return this.ngEventBus.on(name);
  }
}
