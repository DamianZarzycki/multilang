import { Injectable } from "@angular/core";
import { MenuItem } from "primeng/api";

@Injectable()
export class BreadcrumbService {

  items: MenuItem[];

  constructor() {
    this.items = [
      { label: "Learning" },
      { label: "flashcards" },
      { label: "drag'n'drop" }
    ];
  }
}
