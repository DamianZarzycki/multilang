import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class FlashcardService {

  flashcardState = new BehaviorSubject<"default" | "flipped">("default");

  constructor() {
  }

  setFlashcardState(state: "default" | "flipped") {
    this.flashcardState.next(state);
  }

  getFlashcardState() {
    return this.flashcardState.asObservable();
  }

}
