import { Injectable } from "@angular/core";
import { LanguageCodes } from "../../models/Enums/language-codes";

@Injectable()
export class TextToSpeechService {

  private _currentLanguage: LanguageCodes = LanguageCodes.EnUS;
  private _textToRead: string;


  constructor() {
  }

  get textToRead() {
    return this._textToRead;
  }

  set textToRead(value: string) {
    this._textToRead = value;
  }

  get currentLanguageCode() {
    return this._currentLanguage;
  }

  set currentLanguageCode(value: LanguageCodes) {
    this._currentLanguage = value;
  }

  textToSpeech(text?: string) {
    const msg = new SpeechSynthesisUtterance();
    msg.volume = 1;
    msg.rate = 1;
    msg.pitch = 0.8;
    msg.text = text ? text : this.textToRead;
    msg.lang = this.currentLanguageCode;
    window.speechSynthesis.speak(msg);
  }
}
