import { Injectable } from "@angular/core";

@Injectable()
export class SideBarService {

  private _displaySideBar = false;

  constructor() {
  }

  get displaySideBar() {
    return this._displaySideBar;
  }

  set displaySideBar(value: boolean) {
    this._displaySideBar = value;
  }
}
