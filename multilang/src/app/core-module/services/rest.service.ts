import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Flashcard } from "../../models/Interfaces/flashcard";

const tmpUrl = "http://localhost:3000/";

@Injectable()
export class RestService {

  constructor(private http: HttpClient) {
  }

//  GET
  getFlashcards() {
    return this.http.get<Flashcard[]>(`${tmpUrl}flashcards`, {observe: "response"});
  }
//  POST
//  PUT
//  PATCH
//  DELETE
}
