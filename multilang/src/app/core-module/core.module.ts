import { NgModule, Optional, SkipSelf } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthService } from "./services/auth/auth.service";
import { TextToSpeechService } from "./services/text-to-speech.service";
import { SideBarService } from "./services/side-bar.service";
import { BreadcrumbService } from "./services/breadcrumb.service";
import { AuthHeaderInterceptor } from "./services/interceptors/auth-header.interceptor";
import { StaticUrlInterceptor } from "./services/interceptors/static-url.interceptor";
import { RestService } from "./services/rest.service";
import { NgEventBus } from "ng-event-bus";


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    AuthService,
    TextToSpeechService,
    SideBarService,
    BreadcrumbService,
    AuthHeaderInterceptor,
    StaticUrlInterceptor,
    RestService,
    NgEventBus
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        "CoreModule is already loaded. Import it in the AppModule only"
      );
    }
  }
}
